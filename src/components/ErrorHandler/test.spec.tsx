/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { ErrorHandlerProps, ErrorHandler } from './';


// Basic ErrorHandler Test:
it
(
    'ErrorHandler renders correctly',
    () =>
    {
        let errorHandlerProperties: ErrorHandlerProps =
        {
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <ErrorHandler/>
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
