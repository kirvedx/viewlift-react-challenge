/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // INCLUDES
/* jQuery is loaded via CDN */
/* slick (slider library) is loaded via CDN */

 // DEFINES
declare var $: any;


export class SlickSlider
{
    constructor(){}

    /**
     * Instantiates a Slick Slider/Carousel
     *
     * @param className The class name of the root element for a slick slider widget
     */
    render( className: string, settings: any )
    {
        $('.' + className ).slick
        (
            settings
        );

        console.log( "SlickSlider render method finished executing!" );
    }
}