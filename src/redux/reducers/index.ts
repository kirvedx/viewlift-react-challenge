/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { combineReducers } from 'redux';
import snagFilms from './snagfilms-reducer';


// DEFINES
const rootReducer = combineReducers({
    snagFilms
});


// Export it:
export default rootReducer;
