/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES


// DEFINES


// CONSTANTS

/* The loading state, an action for displaying a loader and wiping errors: */
export const FETCH_FILMS_BEGIN = "FETCH_FILMS_BEGIN";

/* The success state, an action for displaying API items: */
export const FETCH_FILMS_SUCCESS = "FETCH_FILMS_SUCCESS";

/* The failure state, an action for displaying errors: */
export const FETCH_FILMS_ERROR = "FETCH_FILMS_ERROR";
