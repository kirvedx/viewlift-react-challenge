/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import fetch from 'cross-fetch';
import {
    FETCH_FILMS_BEGIN,
    FETCH_FILMS_SUCCESS,
    FETCH_FILMS_ERROR
} from "../constants/action-types";


// DEFINES



/* Dispatch Actions for fetching a films list from SnagFilms: */
export const fetchFilmsBegin = () => ( { type: FETCH_FILMS_BEGIN } );

export const fetchFilmsSuccess = ( films: any ) => ( { type: FETCH_FILMS_SUCCESS, payload: { films } } );

export const fetchFilmsError = ( error: any ) => ( { type: FETCH_FILMS_ERROR, payload: { error } } );


/**
 * Method to get a list of 10 movies from SnagFilms API via `fetch`
 *
 * @param { void }
 *
 * @return { Promise<any> }
 *
 * @since 1.0.0
 */
export function fetchFilms()
{
    console.log( 'Fetching films!' );
    return ( dispatch: any ) => {

        dispatch( fetchFilmsBegin() );
        return fetch( "https://www.snagfilms.com/apis/films.json?limit=10" )
        .then( handleErrors )
        .then( response => response.json() )
        .then
        (
            ( json ) =>
            {
                console.log( 'Dispatching success action: ' );
                console.log( json.films );

                dispatch( fetchFilmsSuccess( json.films ) );

                return json.films;
            }
        )
        .catch( ( error ) => dispatch( fetchFilmsError( error ) ) );
    };
}


/**
 * A method to handle HTTP Errors
 *
 * @param { any } response The fetch response
 *
 * @return { any } Returns the response if no errors, else it throws the error
 *
 * @since 1.0.0
 */
function handleErrors( response: any )
{
    if( !response.ok )
    {
        throw Error( response.statusText );
    }

    return response;
}
