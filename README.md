# viewlift-react-challenge

[![pipeline status](https://gitlab.com/devrikx/viewlift-react-challenge/badges/master/pipeline.svg)](https://gitlab.com/devrikx/viewlift-react-challenge/commits/master)  [![coverage report](https://gitlab.com/devrikx/viewlift-react-challenge/badges/master/coverage.svg)](https://devrikx.gitlab.io/viewlift-react-challenge/coverage/)

The viewlift-react-challenge project loads an API response from SnagFilms, within a view styled similarly to that of the SnagFilms homepage, for the end user to review...

## TOC
* [Implementation Details](#implementation-details)
* [Build Instructions](#build-instructions)
  * [Git the Source](#git-the-source)
  * [Install Dependencies](#install-dependencies)
  * [Build the Application](#build-the-application)
  * [Test the application](#test-the-application)
* [Other Considerations](#other-considerations)
  * [Gitflow](#gitflow)
  * [Continuous Integration and Deployment](#continuous-integration-and-deployment)
  * [Viewlift: A React Challenge](#viewlift-a-react-challenge)
* [Tools](#tools)

## Implementation Details

The application shell consists of the following:

* NPM for project dependency management.
* Webpack for source compilation/optimization.
* Gulp for project maintainence.
* Bootstrap/Material Design for complimenting a standard in design.
* React.js as the primary view layer.

### Components

There are some components which are provided with the project template that I've created:

* ErrorHandler - This component is a simple 'whoopsie' view for when an end user hits a 'bad' route.
* UnderConstruction - As the name implies, this component is great as an assignment for any route which is not yet completed, or as an indicator that maintanence is underway.

## Build Instructions

The following steps should be taken in order to build this application:

### Git the Source

Clone the repository from [Gitlab](https://gitlab.com/devrikx/viewlift-react-challenge):

*via HTTPS*:

```bash
git clone https://gitlab.com/devrikx/viewlift-react-challenge
```

*via Git+SSH*:

```bash
git clone git@gitlab.com:devrikx/viewlift-react-challenge
```

### Install dependencies

The following dependencies are required:

* Node.js (latest version is ideal)
  * NPM, which will come pre-bundled with Node.js.

You may use Yarn if you like, however, NPM will suffice.

Head back to your terminal, and proceed to install the additional dependencies as follows:

```bash
cd viewlift-react-challenge
npm install .
```

### Build the Application

Now that you have all the dependencies installed, run the following command to build the application:

```bash
npm run build
```

That's it! You've successfully built the application.

### Test the Application

There are two different ways you can test the application:

#### Development

To test the application as a developer, you may leverage the tests which are provided with the source. The tests are built using [Jest](https://jestjs.io/docs/en/tutorial-react) and [ts-jest](https://github.com/kulshekhar/ts-jest/).

Run the following command:

```bash
npm test
```

Snapshots are provided with the source for the the Jest environment to test against. If you contribute to the code base, make sure that you make any adustments to the tests that are necessary, though, you should rarely need to modify the existing tests. If there *are* any changes to the snapshot(s), you will need to update them and ensure that they are included in your commits. You can do this by either:

* Deleting the `__snapshots__` directories for any components whose snapshots have been modified.
* Running the `jest --updateSnapshot` command to re-record every snapshot that fails.
  * Please note that you'll need to either script this through NPM, or install jest-cli.

#### Production

Open `public/index.html` in your favorite browser (*Chrome recommended*) to test the application.

## Other Considerations

As a compliment to the code itself, I've made use of some common features of Gitlab for performing common project management tasks, as well as in making use of best practices.

To see what I've done, explore the Project Management features of Gitlab in the following ways:

### Gitflow

* The [project board](https://gitlab.com/mmod/viewlift-react-challenge/boards?=) can be visited to explore [user-scenarios/tasks](https://gitlab.com/mmod/viewlift-react-challenge/issues), and [milestones](https://gitlab.com/mmod/viewlift-react-challenge/milestones) which helped to guide completion of the challenge.
  * [Issue-First Development](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) was used for driving the management of the challenge. View the graph [HERE](https://gitlab.com/devrikx/viewlift-react-challenge/network/master)

### Continuous Integration and Deployment

I leveraged the job/pipeline features of Gitlab in the following ways:

* When checking in the master branch, a [pipline](https://gitlab.com/mmod/viewlift-react-challenge/pipelines) is utilized in testing, building, and deploying the challenge application.
  * [Jobs](https://gitlab.com/mmod/viewlift-react-challenge/-/jobs) are dispatched with every commit to master; Test and Deploy. [Here](https://gitlab.com/mmod/viewlift-react-challenge/blob/master/.gitlab-ci.yml)'s the configuration.

### Viewlift: A React Challenge

You can review the [Viewlift Challenge React Application](https://devrikx.gitlab.io/viewlift-react-challenge/), continuously deployed to Gitlab pages upon a check-in to master.

## Tools

The tools leveraged in building this template include:

* [Visual Studio Code](https://code.visualstudio.com/)